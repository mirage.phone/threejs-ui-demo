# Install npm
npm i

# Run the local server
npm run dev

# Build for production in the dist/ directory
npm run build
