import * as THREE from 'three';
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import { FontLoader } from 'three/addons/loaders/FontLoader.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';


//создание сцены
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
    50,
    window.innerWidth/window.innerHeight,
    0.1,
    1000
);
const renderer = new THREE.WebGL1Renderer();
renderer.setSize(window.innerWidth, window.innerHeight);
scene.background = new THREE.Color( 0x282828 );
document.body.appendChild( renderer.domElement );



//загружаем и читаем файл
const loaderJson = new THREE.FileLoader();
const loaderText = new FontLoader();
let ID_mesh, Name_mesh, Number_mesh, Value_mesh;
let idMesh, nameMesh, numberMesh, valueMesh;
let infoMesh;
loaderJson.load(
	'/json/accounts.json',
	
	function ( data ) {
        let text = JSON.parse(data);
        loaderText.load( 'fonts/Smallest Pixel-7_Regular.json', 
        function ( font ) {
            document.getElementById('check').onclick = function() {
                let num = document.getElementById('input-id').value;
                if(num < 6){
                    scene.remove(infoMesh);
                    scene.remove(ID_mesh, Name_mesh, Number_mesh, Value_mesh);
                    scene.remove(idMesh, nameMesh, numberMesh, valueMesh);
                    const idGeometry = new TextGeometry( text[num-1].id, {
                        font: font,
                        size: 0.1,
                        height: 0.03
                    } );
                    //запись данных
                    const nameGeometry = new TextGeometry( text[num-1].name, { font: font, size: 0.1, height: 0.03} );
                    const numberGeometry = new TextGeometry( text[num-1].number, { font: font, size: 0.1, height: 0.03} );
                    const valueGeometry = new TextGeometry( text[num-1].value, { font: font, size: 0.1, height: 0.03} );
                    idMesh = new THREE.Mesh(idGeometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    nameMesh = new THREE.Mesh(nameGeometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    numberMesh = new THREE.Mesh(numberGeometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    valueMesh = new THREE.Mesh(valueGeometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    idMesh.position.set(0, 0.6, 0);
                    nameMesh.position.set(0, 0.3, 0);
                    numberMesh.position.set(0, 0, 0);
                    valueMesh.position.set(0, -0.3, 0);
                    scene.add(idMesh, nameMesh, numberMesh, valueMesh);
                    //вывод описания
                    const ID_geometry = new TextGeometry( 'ID: ', { font: font, size: 0.1, height: 0.03 } );
                    const Name_geometry = new TextGeometry( 'Name: ', { font: font, size: 0.1, height: 0.03 } );
                    const Number_geometry = new TextGeometry( 'Number: ', { font: font, size: 0.1, height: 0.03 } );
                    const Value_geometry = new TextGeometry( 'Value: ', { font: font, size: 0.1, height: 0.03 } );
                    ID_mesh = new THREE.Mesh(ID_geometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    Name_mesh = new THREE.Mesh(Name_geometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    Number_mesh = new THREE.Mesh(Number_geometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    Value_mesh = new THREE.Mesh(Value_geometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    ID_mesh.position.set(-0.8, 0.6, 0);
                    Name_mesh.position.set(-0.8, 0.3, 0);
                    Number_mesh.position.set(-0.8, 0, 0);
                    Value_mesh.position.set(-0.8, -0.3, 0);
                    scene.add(ID_mesh, Name_mesh, Number_mesh, Value_mesh);
                    
                }
                if(num >= 6){
                    scene.remove(infoMesh);
                    scene.remove(ID_mesh, Name_mesh, Number_mesh, Value_mesh);
                    scene.remove(idMesh, nameMesh, numberMesh, valueMesh);
                    const tGeometry = new TextGeometry( "Введите число от 1 до 5", { font: font, size: 0.4, height: 0.08
                    } );
                    infoMesh = new THREE.Mesh(tGeometry, new THREE.MeshBasicMaterial({color: 0xffffff}));
                    infoMesh.position.set(-3,0,0);
                    scene.add(infoMesh);
                }
            }
            
        } );
	}
);


//вращение камеры
const controls = new OrbitControls( camera, renderer.domElement );
camera.position.z = 5;
controls.update();


function render(){
    requestAnimationFrame(render);
    controls.update();
    renderer.render(scene, camera);
}
render();